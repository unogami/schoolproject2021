#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# fish.py

from tkinter import *
from tkinter import ttk

STR_CALCULATE = 'Вычислить!'
STR_o_O = 'Целевая концентрация слишком мала!'
STR_O_o = 'Целевая концентрация слишком велика!'
STR_Fish_method = 'Метод "Рыбка" Магницкого'
# https://unicode-table.com/en/sets/arrow-symbols/
# STR_target_concentration = '← Целевая концентрация'
STR_target_concentration = 'Целевая концентрация:'

ROW_SOL1 = 2
ROW_TARGET = ROW_SOL1 + 3
ROW_SOL2 = ROW_TARGET + 3
ROW_SOL1_PART = ROW_SOL1 + 2
ROW_SOL2_PART = ROW_SOL2 - 2
ROW_SOL1_RES = ROW_SOL1
ROW_SOL2_RES = ROW_SOL2
ROW_MENU = ROW_SOL1 - 1

ROW_CALC_BUTTON = ROW_SOL2 +2
COL_CALC_BUTTON = 2

COL_TARGET = 1  # shift right
COL_SOL = COL_TARGET + 3
COL_SOL1 = COL_SOL2 = COL_SOL
COL_SOL_PART = COL_SOL + 3
COL_SOL1_PART = COL_SOL2_PART = COL_SOL_PART
COL_SOL_RES = COL_SOL_PART + 5
COL_SOL1_RES = COL_SOL2_RES = COL_SOL_RES
COL_MENU = COL_TARGET - 1

COL_SLASH_1 = COL_SOL_PART - 1
COL_SLASH_2 = COL_SOL_PART + 1
ROW_SLASH_1 = ROW_SOL1_PART - 1
ROW_SLASH_2 = ROW_SOL2_PART + 1

def float_or_none(str):
    try:
        return float(str)
    except:
        return None

def calculate(*args):
    try:
        t = float(target.get())
        s1 = float(sol1.get())
        s2 = float(sol2.get())

        # Check validity
        if t < min(s1, s2):
            sol1_res.set(STR_o_O)
            return
        if t > max(s1, s2):
            sol1_res.set(STR_O_o)
            return

        diff1 = abs(s1 - t)
        diff2 = abs(s2 - t)
        sol1_part.set(diff1)
        sol2_part.set(diff2)

        total_diff = diff1 + diff2 # == abs(s1 - s2)

        sol1_share = diff2 / total_diff
        sol2_share = diff1 / total_diff
        sol1_res.set(f'{sol1_share*100:f}%')
        sol2_res.set(f'{sol2_share*100:f}%')

        # Now with masses
        mt = float_or_none(target_mass.get())
        s1m = float_or_none(sol1_mass.get())
        s2m = float_or_none(sol2_mass.get())

        if mt is not None and s1m is None and s2m is None:
            sol1_mass.set(mt * sol1_share)
            sol2_mass.set(mt * sol2_share)
            return

        if mt is None and s1m is not None: # and s2m is  None:
            mt = s1m/sol1_share
            s2m = mt * sol2_share
            target_mass.set(mt)
            sol1_mass.set(s1m)
            sol2_mass.set(s2m)
            return

        if mt is None and s2m is not None: # and s2m is  None:
            mt = s2m/sol2_share
            s1m = mt * sol1_share
            target_mass.set(mt)
            sol1_mass.set(s1m)
            sol2_mass.set(s2m)
            return

    except ValueError as e:
        print(e)
        pass

# Example:
# preset == [['Name', 'Description'], [target, 40], [sol1, 0], [sol2, 96]]
def use_preset(preset):
    for pair in preset:
        object, value = pair
        if isinstance(object, str):  # First pair is Name and Description
            print(f'{object} {value}')
            continue
        print(object, value)
        object.set(value)


root = Tk()
root.title(STR_Fish_method)

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

#Labels

# Add percent sign to the fields, at right
percent_char = StringVar()
percent_char.set('%')  # Can change it
ttk.Label(mainframe, textvariable=percent_char).grid(column=COL_TARGET+1  , row=ROW_TARGET, sticky=(W, E))
ttk.Label(mainframe, textvariable=percent_char).grid(column=COL_SOL+1  , row=ROW_SOL1, sticky=(W, E))
ttk.Label(mainframe, textvariable=percent_char).grid(column=COL_SOL+1  , row=ROW_SOL2, sticky=(W, E))

ttk.Label(mainframe, text=STR_target_concentration).grid(column=COL_TARGET-1, row=ROW_TARGET, sticky=E)
ttk.Label(mainframe, text='Общая масса:').grid(column=COL_TARGET-1, row=ROW_TARGET +1, sticky=E)

ttk.Label(mainframe, text='Первый сплав/раствор').grid(column=COL_SOL , row=ROW_SOL1-1, sticky=(W, E))
ttk.Label(mainframe, text='Второй сплав/раствор').grid(column=COL_SOL , row=ROW_SOL2-1, sticky=(W, E))

mass_word = StringVar()
mass_word.set('масса:')  # Can change it
ttk.Label(mainframe, textvariable=mass_word).grid(column=COL_SOL-1  , row=ROW_SOL1+1, sticky=(W, E))
ttk.Label(mainframe, textvariable=mass_word).grid(column=COL_SOL-1  , row=ROW_SOL2+1, sticky=(W, E))

concentration_word = StringVar()
concentration_word.set('концентрация:')  # Can change it
ttk.Label(mainframe, textvariable=concentration_word).grid(column=COL_SOL-1  , row=ROW_SOL1, sticky=(W, E))
ttk.Label(mainframe, textvariable=concentration_word).grid(column=COL_SOL-1  , row=ROW_SOL2, sticky=(W, E))


ttk.Label(mainframe, text='\\').grid(column=COL_SLASH_1 , row=ROW_SLASH_1, sticky=(W, E))
ttk.Label(mainframe, text='\\').grid(column=COL_SLASH_2 , row=ROW_SLASH_2, sticky=(W, E))
ttk.Label(mainframe, text='/').grid(column=COL_SLASH_1 , row=ROW_SLASH_2, sticky=(W, E))
ttk.Label(mainframe, text='/').grid(column=COL_SLASH_2 , row=ROW_SLASH_1, sticky=(W, E))

# ^^^Labels

target = StringVar()
target_entry = ttk.Entry(mainframe, width=7, textvariable=target)
target_entry.grid(column=COL_TARGET, row=ROW_TARGET, sticky=(W, E))

target_mass = StringVar()
target_mass_entry = ttk.Entry(mainframe, width=7, textvariable=target_mass)
target_mass_entry.grid(column=COL_TARGET, row=(1+ROW_TARGET), sticky=(W, E))

sol1 = StringVar()
sol1_entry = ttk.Entry(mainframe, width=7, textvariable=sol1)
sol1_entry.grid(column=COL_SOL1, row=ROW_SOL1, sticky=(W, E))

sol2 = StringVar()
sol2_entry = ttk.Entry(mainframe, width=7, textvariable=sol2)
sol2_entry.grid(column=COL_SOL2, row=ROW_SOL2, sticky=(W, E))

sol1_mass = StringVar()
sol1_mass_entry = ttk.Entry(mainframe, width=7, textvariable=sol1_mass)
sol1_mass_entry.grid(column=COL_SOL1, row=(1+ROW_SOL1), sticky=(W, E))

sol2_mass = StringVar()
sol2_mass_entry = ttk.Entry(mainframe, width=7, textvariable=sol2_mass)
sol2_mass_entry.grid(column=COL_SOL2, row=(1+ROW_SOL2), sticky=(W, E))

sol1_part = StringVar()
ttk.Label(mainframe, textvariable=sol1_part).grid(column=COL_SOL1_PART, row=ROW_SOL1_PART, sticky=(W, E))

sol2_part = StringVar()
ttk.Label(mainframe, textvariable=sol2_part).grid(column=COL_SOL2_PART, row=ROW_SOL2_PART, sticky=(W, E))

sol1_res = StringVar()
ttk.Label(mainframe, textvariable=sol1_res).grid(column=COL_SOL1_RES, row=ROW_SOL1_RES, sticky=(W, E))

sol2_res = StringVar()
ttk.Label(mainframe, textvariable=sol2_res).grid(column=COL_SOL2_RES, row=ROW_SOL2_RES, sticky=(W, E))

ttk.Button(mainframe, text=STR_CALCULATE, command=calculate).grid(column=COL_CALC_BUTTON, row=ROW_CALC_BUTTON, sticky=W)

preset_list = [
    [['Ничего', 'Очистить все поля'],
     [target, ''], [sol1, ''], [sol2, ''], [target_mass, ''], [sol1_mass, ''], [sol2_mass, ''],
     [sol1_res, ''], [sol2_res, ''], [sol1_part, ''], [sol2_part, '']
     ],
    [['Солевой раствор', 'Пример из текста'], [target, 8], [sol1, 10], [sol2, 5], [target_mass, ''], [sol1_mass, 15], [sol2_mass, '']],
    [['Спирт и вода', 'Смешивание'], [target, 40], [sol1, 0], [sol2, 96], [target_mass, ''], [sol1_mass, ''], [sol2_mass, '']],
    [['Дезраствор', ''], [target, 0.5], [sol1, 0], [sol2, 2], [target_mass, ''], [sol1_mass, ''], [sol2_mass, '']],
    [['Молоко', ''], [target, 2.5], [sol1, 0.5], [sol2, 4], [target_mass, 200], [sol1_mass, ''], [sol2_mass, '']],
    [['Сено и трава', ''], [target, 60], [sol1, 20], [sol2, 100], [target_mass, 1000], [sol1_mass, ''], [sol2_mass, '']],
]

default_preset_index = 1 # The 'Salt solution'

OptionList = [x[0][0] for x in preset_list]
print(OptionList)

variable = StringVar(root)
variable.set(OptionList[default_preset_index])

def callback(*args):
    selected_option = variable.get()
    print(selected_option)
    # Find the option (May use hash for speed, but not necessary)
    for preset in preset_list:
        name, desc = preset[0]
        print(name, desc)
        if name == selected_option:
            use_preset(preset)

variable.trace("w", callback)

opt = OptionMenu(mainframe, variable, *OptionList)
# opt.config(width=90, font=('Helvetica', 12))
opt.grid(column=COL_MENU, row=(ROW_MENU), sticky=(W, E))

for child in mainframe.winfo_children():
    child.grid_configure(padx=5, pady=5)

use_preset(preset_list[default_preset_index])

target_entry.focus()
root.bind("<Return>", calculate)

root.mainloop()
